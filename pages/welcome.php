
<center>
        <pre id="logo"><?php echo file_get_contents('../assets/doom_logo.txt'); ?></pre>
        <div id="menu">
            <div class="menu__action">
                <span class="menu__action__description">
                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                    Open File
                </span>
                <span class="menu__action__command">SPC f f</span>
            </div>
            <div class="menu__action">
                <span class="menu__action__description">Help</span>
                <span class="menu__action__command">SPC h c</span>
            </div>
        </div>
        <em>
            Le but de ce site est de reproduire l'utilisation primaire d'emacs. Vous pourrez donc accéder aux différents fichiers du site via les command de Dired par exemple. Amusez-vous !
        </em>
    </center>
