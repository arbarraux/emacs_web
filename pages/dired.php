<!DOCTYPE html>
<html>
  <head>
    <?php include "../includes/head.html"; ?>
    <script type="text/javascript" src="../js/dired.js"></script>
  </head>
  <body class="dired">
<?php
    $ls = explode("\n", shell_exec("ls -l ".$_GET['dir']));
    $pwd = shell_exec("cd " .$_GET["dir"] ." ; pwd");
    $pwd = str_replace("\n", "", $pwd);
    echo "<div><span class='dired__title__count'>*/" .count($ls) ." Dired (directory) </span><span class='dired__title__pwd'>" . $pwd ."</span></div><br/>";
    for ($i=1; $i < count($ls)-1; $i++){
        if ($i==1) {
            echo "<div id=$i class='dired__item dired__item__active'>$ls[$i]</div>";
        } else {
            echo "<div id=$i class='dired__item'>$ls[$i]</div>";
        }
    }
?>

  </body>
</html>
