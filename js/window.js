buffer = []
list_command = [
    "Control x d",
    "SPC f f",
    "SPC h c"
]

document
    .addEventListener("keydown", (event) => {
        switch (event.key) {
        case "Escape":
            buffer = []
            break;
        case " ":
            buffer.push("SPC");
            break;
        default:
            buffer.push(event.key);
            break;
        }

        switch(buffer.join(" ")){
        case "Control x d":
            buffer = [];
            dired();
            break;
        case "SPC f f":
            buffer = [];
            dired();
            break;
        case "SPC h c":
            buffer = [];
            help();
            break;
        }
        parent.document.getElementById("command-buffer").innerText = buffer.join(" ");
    });


function dired() {
    frame = parent.document.getElementById("dired");
    console.log(frame);
    frame.style.display = "block";
    frame.contentWindow.focus();
}

function move(direction) {
    active = document.getElementsByClassName("dired__item__active")[0];
    dired_item = document.getElementsByClassName("dired__item");
    active_index = active.id;
    if (direction == 'up' && active_index > 3){
        active.classList.remove("dired__item__active");
        dired_item[active_index - 4].classList.add("dired__item__active");
    } else if (direction == 'down' && active_index < dired_item.length +2){
        active.classList.remove("dired__item__active");
        dired_item[active_index - 2].classList.add("dired__item__active");
    }
}

function help() {

    path = parent.document
        .getElementById("main_frame").contentWindow
        .location.href.split('?');
    path.pop();
    uri = path.join('') + '?file=help.html'
    parent.document
        .getElementById("main_frame").contentWindow
        .location.href = encodeURI(uri);
}
