buffer = []
list_command = [
    "Control x d",
    "  f f"
]

document
    .addEventListener("keydown", (event) => {
        if (event.defaultPrevented) {
            return;
        }
        document.getElementById("command-buffer").innerText = buffer.join(" ");
        switch (event.key) {
        case 'h':
            break;
        case 'j':
            break;
        case 'k':
            break;
        case 'l':
            break;
        case "Escape":
            buffer = []
            break;
        default:
            buffer.push(event.key);
            break;
        }

        switch(buffer.join(" ")){
        case "Control x d":
            buffer = [];
            dired();
            break;
        case "  f f":
            buffer = [];
            dired();
            break;
        }
    });


function dired() {
    frame = document.getElementById("dired");
    console.log(frame);
    frame.style.display = "block";
    frame.contentWindow.focus();
}

function move(direction) {
    active = document.getElementsByClassName("dired__item__active")[0];
    dired_item = document.getElementsByClassName("dired__item");
    active_index = active.id;
    if (direction == 'up' && active_index > 3){
        active.classList.remove("dired__item__active");
        dired_item[active_index - 4].classList.add("dired__item__active");
    } else if (direction == 'down' && active_index < dired_item.length +2){
        active.classList.remove("dired__item__active");
        dired_item[active_index - 2].classList.add("dired__item__active");
    }
}
