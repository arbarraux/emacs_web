buffer = []
list_command = [
    "Control x d",
    "SPC f f"
]

document
    .addEventListener("keydown", (event) => {
        if (event.defaultPrevented) {
            return;
        }

        switch (event.key) {
        case 'j':
            move("down");
            break;
        case 'k':
            move("up");
            break;
        case "Enter":
            open();
            break;
        case "-":
            open("..");
            break;
        case "Escape":
            buffer = []
            close_dired();
            break;
        case " ":
            buffer.push("SPC");
            break;
        default:
            buffer.push(event.key);
            break;
        }

        switch(buffer.join(" ")){
        case "Control x d":
            buffer = [];
            break;
        case "  f f":
            buffer = [];
            break;
        }
    });


function move(direction) {
    active = document.getElementsByClassName("dired__item__active")[0];
    dired_item = document.getElementsByClassName("dired__item");
    console.log(dired_item);
    active_index = parseInt(active.id);
    console.log(direction);
    console.log(active_index);
    console.log(dired_item.length);
    if (direction == 'up' && active_index > 1){
        active.classList.remove("dired__item__active");
        dired_item[active_index -2].classList.add("dired__item__active");
    } else if (direction == 'down' && active_index < dired_item.length){
        console.log("hello");
        active.classList.remove("dired__item__active");
        dired_item[active_index].classList.add("dired__item__active");
    }
}

function open(dir=null){
    if (dir != null){
        path = window.location.href.split('?');
        dir = document.getElementsByClassName("dired__title__pwd")[0].innerHTML.split('/');
        console.log("dir:", dir);
        path.pop();
        dir.pop();
        dir = dir.join('/');
        if (!dir.includes("/var/www/html/emacs")){
            dir = "/var/www/html/emacs";
        }
        uri = path.join('') + '?dir='+ dir;
        console.log("uri: ", uri);
        window.location.href = encodeURI(uri);
        return;
    }
    // If is not a directory
    if (document.getElementsByClassName("dired__item__active")[0].innerHTML.slice(0, 1) != 'd'){
        file_name = document.getElementsByClassName("dired__item__active")[0].innerHTML.split(' ').pop();
        dir = document.getElementsByClassName("dired__title__pwd")[0].innerHTML + '/';
        path = parent.document
            .getElementById("main_frame").contentWindow
            .location.href.split('?');
        path.pop();
        uri = path.join('') + '?file=' + dir + file_name
        parent.document
            .getElementById("main_frame").contentWindow
            .location.href = encodeURI(uri);
    } else { // If it is
        dir_name = document.getElementsByClassName("dired__item__active")[0].innerHTML.split(' ').pop() + '/';
        dir = document.getElementsByClassName("dired__title__pwd")[0].innerHTML + '/';
        console.log(dir, dir_name);
        path = window.location.href.split('?');
        path.pop();
        uri = path.join('') + '?dir=' + dir +dir_name
        window.location.href = encodeURI(uri);

    }
}

function close_dired() {
    parent.document
        .getElementById("dired")
        .style.display = "none";
    parent.document
        .getElementById("main_frame")
        .contentWindow.focus();
}
